'use strict'

var gulp = require('gulp'),
    sass = require('gulp-sass');
    sass.compiler = require('node-sass');
var browserSync = require('browser-sync'),
    browserSync = require('browser-sync').create();   



var del      = require('del');
var imagemin = require('gulp-imagemin');
var uglify   = require('gulp-uglify');
var usemin   = require('gulp-usemin');
var rev      = require('gulp-rev');
var cleanCss = require('gulp-clean-css');
var flatmap  = require('gulp-flatmap');
var htmlmin  = require('gulp-htmlmin');


//configurar tareas para compilar sass
gulp.task('sass', function(){
   return gulp.src('./css/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./css'));
});

// Watchers
gulp.task('watch', function() {
    gulp.watch('./css/*.scss', gulp.series('sass'));
    gulp.watch('./*.html', browserSync.reload);
    gulp.watch('./js/*.js', browserSync.reload);
});

// Start browserSync server
gulp.task('browserSync', function() {
    var files =['./*.html','./css/*.css','/images/*.{png,jpg,gif,jpeg}','/images/Propaganda/*.{png,jpg,gif,jpeg}','./js/*.js']
    browserSync.init(files,{
      server: {
        baseDir: './'
      }
    })
});

//tarea default que monitorea y que ejeucta el servidor solo se ejeucta gulps
gulp.task('default',gulp.series('browserSync', function(){
    gulp.start('watch');    
    })
);
//********************************* */
//**********BUILD****************** */
//********************************* */


//cofiguar tare borrado
gulp.task('clean', function() {
    return del(['dist']);
});

gulp.task('copyfonts',function(){
   return gulp.src('./node_modules/open-iconic/font/fonts/*.{tff,woff,eof,svg,eot,otf}*')
    .pipe(gulp.dest('./dist/fonts'));
});

//minificar imagenes
gulp.task('imagemin',function(){
    return gulp.src('images/**/*.{png,jpg,gif,jpeg}')
    .pipe(imagemin({
            optimizationLevel: 3, 
            progressive      : true,
            interlaced       : true})
            ).pipe(gulp.dest('dist/images'));
});
//flujo de archivos a procesar
gulp.task('usemin',function(){
    return gulp.src('./*.html')
                .pipe(flatmap(function(stream,file){
                    return stream
                        .pipe(usemin({
                            css       : [rev()],
                            html      : [function(){return htmlmin({
                                         collapseWhitespace:true
                                         })}],
                            js        : [uglify(),rev()],
                            inlinejs  : [uglify()],
                            inlinecss : [cleanCss(),'concat']
                        }));
                }))
                .pipe(gulp.dest('dist/'));

});

var build = gulp.parallel('clean','copyfonts', 'imagemin','usemin');

gulp.task('build', build);