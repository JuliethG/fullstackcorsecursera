import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from '../models/destino-viaje.models';
import { FormGroup, Form, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { map,filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  fg: FormGroup;
  minLongNombre = 3;
  searchResult: string[];


  constructor(public fb:FormBuilder) { //permite definir y construir grupo
    this.onItemAdded = new EventEmitter();
    this.fg          = fb.group({ //validator
      nombre   :['',Validators.compose([
                    Validators.required,
                    this.nombreValidator,
                    this.nombreValidatorParametrizable(this.minLongNombre)
                    ])],
      urlImagen:['',Validators.required]
    });

    this.fg.valueChanges.subscribe((form : any)=>{
      console.log("cambio el formulario " ,form);
    });
   }

  ngOnInit(): void {
    let elemNombre = <HTMLInputElement>document.getElementById('nombre');
    fromEvent(elemNombre,'input')//esucha las teclas es un observable
    .pipe(
      map((e:KeyboardEvent) => (e.target as HTMLInputElement).value),
      filter(text => text.length > 4),
      debounceTime(200),
      distinctUntilChanged(),
      switchMap(() => ajax('/assets/data.json')) //simular una consulta de web services peroe s texto statico
    ).subscribe(ajaxResponse => {
        this.searchResult = ajaxResponse.response;        
    });
  }

  guardar(nombre:string,urlImagen:string):boolean{
    let d= new DestinoViaje(nombre,urlImagen);
    this.onItemAdded.emit(d);
    return false;
  }

  nombreValidator(control:FormControl):{ [ control : string]: boolean } {
    let l = control.value.toString().trim().length;
    if(l > 0 && l < 5){
      return { invalidNombre : true };
    }
    return null;
  }

  //validador paramtrizable
  nombreValidatorParametrizable(minLong:number):ValidatorFn{
    return ( control:FormControl):{[control:string]:boolean}| null =>{
      let l = control.value.toString().trim().length;
      if(l > 0 && l < minLong){
        return { minLongNombre : true };
      }
      return null;
    }
  }


}
