import {v4 as uuid} from 'uuid';

export class DestinoViaje {
  selected: boolean;
  servicios: String[];
  id = uuid();
  constructor(public nombre: String, public urlImagen: String) {
    this.servicios = ["pileta", "desayunos"];
  }

  isSelected(): boolean {
    return this.selected;
  }
  setSelected(isSelect: boolean): void {
    this.selected = isSelect;
  }

}