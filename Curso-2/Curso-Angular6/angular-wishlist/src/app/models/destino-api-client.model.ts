import {  DestinoViaje } from './destino-viaje.models';
import { Subject, BehaviorSubject } from 'rxjs';

export class DestinoApiclient {
  destinos: DestinoViaje[];
  current : Subject<DestinoViaje> = new  BehaviorSubject<DestinoViaje>(null);//objeto observable y que mira los estados(tipo notificacion)

  constructor() {
    this.destinos = [];
  }

  add(destinos: DestinoViaje) {
    this.destinos.push(destinos);
  }

  getAll(): DestinoViaje[] {
    return this.destinos;
  }

  getById(id: string): DestinoViaje {
    return this.destinos.filter(function (data) {
      if (data.id.toString() == id) {
        return data;
      }
    })[0];
  }

  elegir(destino:DestinoViaje){
      this.destinos.forEach(x=> x.setSelected(false));
      destino.setSelected(true);
      this.current.next(destino);
  }

  /**
   * manera de subs
   * @param fn 
   */
  subscribeOnChange(fn){
    this.current.subscribe(fn);
  }

}