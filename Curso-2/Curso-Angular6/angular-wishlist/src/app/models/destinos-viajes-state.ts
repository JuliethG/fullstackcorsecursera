import { Injectable  } from "@angular/core";
import { Action  } from "@ngrx/store";
import { Actions, Effect, ofType  } from "@ngrx/effects";
import { Observable , of} from "rxjs";
import { map  } from "rxjs/operators";
import { DestinoViaje } from './destino-viaje.models';


//estados
export interface DestinosViajesState {
    items:DestinoViaje[];
    loading:boolean;
    favorite:DestinoViaje;
}

export const initializedestinoViajesState=function(){
    return{
        items:[],
        loading:false,
        favorite:null
    };
};
// ACCIONS DISPARADAS POR EL USUARIO
export enum DestinoViajesActionTypes{
    NUEVO_DESTINO    = '[Destino viaje] Nuevo',
    ELEGIDO_FAVORITO = '[Destino viaje] Favorito'
}

export class NuevoDestinoAction implements Action{
    type =DestinoViajesActionTypes.NUEVO_DESTINO;
    constructor(public destino:DestinoViaje){}
}

export class ElegidoFavoritoAction implements Action{
    type =DestinoViajesActionTypes.ELEGIDO_FAVORITO;
    constructor(public destino:DestinoViaje){}
}
// agrupar las diferentes tipos de acciones
export type DestinoViajesActions = NuevoDestinoAction | ElegidoFavoritoAction ;

//REDUCERS= se encarga de disparar la accion y recibe lo
export function reducerDestinosViajes (
    state : DestinosViajesState,
    action: DestinoViajesActions
):DestinosViajesState{
    switch(action.type){
      case DestinoViajesActionTypes.NUEVO_DESTINO:{
           return{  
               ...state,
               items:[...state.items,(action as NuevoDestinoAction).destino]
           };
      }
      case DestinoViajesActionTypes.ELEGIDO_FAVORITO:{
          state.items.forEach(x => x.setSelected(false));
          let fav: DestinoViaje =(action as ElegidoFavoritoAction).destino
          fav.setSelected(true);
          return{
              ...state,
              favorite:fav
          };
      }
    }
    return state;
}

//efects registrar una nueva accion en concecuencia de otra opcion
@Injectable()
export class DestinoViajeEffects{
     @Effect()
     nuevoAgregado$: Observable<Action> = this.actions$.pipe(
        ofType(DestinoViajesActionTypes.NUEVO_DESTINO),
        map((action:NuevoDestinoAction) => new ElegidoFavoritoAction(action.destino))
     );
    constructor(private actions$:Actions){}
}