import { Component, OnInit, Output,EventEmitter } from '@angular/core';
import { DestinoViaje} from './../models/destino-viaje.models';
import { DestinoApiclient } from './../models/destino-api-client.model';


@Component({
  selector: 'app-lista-destino',
  templateUrl: './lista-destino.component.html',
  styleUrls: ['./lista-destino.component.css']
})
//lista de destino
export class ListaDestinoComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  update:string[];

  constructor(public destinoApiclient:DestinoApiclient) {    
    this.onItemAdded = new EventEmitter();    
    this.update = [];
    this.destinoApiclient.subscribeOnChange((d:DestinoViaje)=>{
      if(d!=null){
          this.update.push('Se ha elegido a'+ d.nombre);
      }
    });
   }

  ngOnInit(): void {
  }

  agregado(destinonuevo:DestinoViaje){
    this.destinoApiclient.add(destinonuevo);
    this.onItemAdded.emit(destinonuevo);
   
  }
  
  elegido(destinoSeleccionado:DestinoViaje):void{
    this.destinoApiclient.elegir(destinoSeleccionado);    
  }

}
