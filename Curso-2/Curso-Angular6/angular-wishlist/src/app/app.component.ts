import { Component } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-wishlist';
  time = new Observable(observer =>{ //observable que le importa o le notifiquen el next
    setInterval(()=>observer.next(new Date().toString()),1000);
  });
  //rendereo diferido



}
