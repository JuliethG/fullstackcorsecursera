import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule,Routes } from '@angular/router';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { StoreModule as NgRxStoreModule ,ActionReducerMap} from '@ngrx/store'
import { EffectsModule } from'@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { AppComponent } from './app.component';
import { DestinoViajeComponent } from './destino-viaje/destino-viaje.component';
import { ListaDestinoComponent } from './lista-destino/lista-destino.component';
import { DestinoDetalleComponent } from './destino-detalle/destino-detalle.component';
import { FormDestinoViajeComponent } from './form-destino-viaje/form-destino-viaje.component';
import { DestinoApiclient } from './models/destino-api-client.model';
import { DestinosViajesState, reducerDestinosViajes, initializedestinoViajesState, DestinoViajeEffects } from './models/destinos-viajes-state';



const routes : Routes = [
  { path: ''    ,   redirectTo : "home", pathMatch:'full'},
  { path: 'home',    component : ListaDestinoComponent   },
  { path: 'destino', component : DestinoDetalleComponent }
];

// reduc init
//definir estados de la aplicacion
export interface AppState{
  destinos:DestinosViajesState;
};
//definir reducer globales
const reducers:ActionReducerMap<AppState> ={
  destinos:reducerDestinosViajes
};
//inicializar 
let reducersInitialState={
  destinos: initializedestinoViajesState()
}
// reduc fin init


@NgModule({
  declarations: [
    AppComponent,
    DestinoViajeComponent,
    ListaDestinoComponent,
    DestinoDetalleComponent,
    FormDestinoViajeComponent    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes)
    /*NgRxStoreModule.forRoot(reducers, { initialState:  reducersInitialState}),
    EffectsModule.forRoot([DestinoViajeEffects])*/
  ],
  providers: [
    DestinoApiclient
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
